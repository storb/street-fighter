import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import App from './app'
import Fighter from './fighter'

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
    this.fightersDetailsMap = new Map();

    App.buttonFight.addEventListener('click', event => this.handleFight(event), false);
  }
  
  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    App.detailElement.style.visibility = 'visible';

    const { _id } = fighter;

    if (!this.fightersDetailsMap.has(_id)) {
      const fighterDetail = await fighterService.getFighterDetails(_id);
      this.fightersDetailsMap.set(_id, fighterDetail);
    }

    while (App.detailElement.firstChild) {
      App.detailElement.removeChild(App.detailElement.firstChild);      
    }

    // Detail information about Fighter
    let fieldsDetail = ['name', 'health', 'attack', 'defense'];
    let detailElements = fieldsDetail.map(field =>{
      let elementWrap = this.createElement({ tagName: 'div', className: 'detail-group' });

      let elementCaption = this.createElement({ tagName: 'span', className: 'detail-caption' });
      elementCaption.innerText =  field.toUpperCase();
      
      let elementItem = this.createElement({ tagName: 'span', className: 'detail-item' });
      elementItem.innerText = this.fightersDetailsMap.get(_id)[field];
      
      elementWrap.append(elementCaption, elementItem);
      return elementWrap;
    })
    
    App.detailElement.append(...detailElements);
  }

  handleFight(event){
    // console.log(this.fightersDetailsMap)

    let selectedFightersItem = document.querySelectorAll('.check-item')
    
    this.buttlePersonsId = [];
    for (let i = 0; i < selectedFightersItem.length; ++i) {
      if (selectedFightersItem[i].checked) {
        this.buttlePersonsId.push(selectedFightersItem[i].getAttribute('dataid'));
      }      
    }

    if (this.buttlePersonsId.length >= 2){
      const firstFighterID = this.buttlePersonsId[0];
      const firstFDetail = this.fightersDetailsMap.get(firstFighterID)

      let firstFighter = new Fighter({ 
        name: firstFDetail.name, 
        health: firstFDetail.health,
        attack: firstFDetail.attack,
        defense: firstFDetail.defense
      });

      const secondFighterID = this.buttlePersonsId[1];
      const secondFDetail = this.fightersDetailsMap.get(secondFighterID)

      let secondFighter = new Fighter({
        name: secondFDetail.name,
        health: secondFDetail.health,
        attack: secondFDetail.attack,
        defense: secondFDetail.defense
      });

      const winnerFighter = this.fight(firstFighter, secondFighter);
      
      let elementItem = this.createElement({ tagName: 'span', className: 'detail-item' });
      elementItem.innerText = "Winner: " + winnerFighter.name;

      while (App.detailElement.firstChild) {
        App.detailElement.removeChild(App.detailElement.firstChild);
      }

      App.detailElement.append(elementItem);

      // console.log("WIN", winnerFighter.name);
    }
    
  }

  fight(firstFighter, secondFighter){

    // const firstAttack = firstFighter.randomInteger(0,1);

    while ((firstFighter.getHealth() > 0) && (secondFighter.getHealth() > 0)) {
        firstFighter.doAttack(secondFighter);
        secondFighter.doAttack(firstFighter);
    }

    if (firstFighter.getHealth() > 0) {
      return firstFighter;
    }

    if (secondFighter.getHealth() > 0) {
      return secondFighter;
    }
  }
}

export default FightersView;