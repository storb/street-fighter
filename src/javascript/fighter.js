class Fighter {

  constructor({ name, health, attack, defense}){
    this.name = name;
    this.health = health;
    this.attack = attack;
    this.defense = defense;  
  }

  getHitPower(){
    let criticalHitChance = this.randomInteger(1,2);
    let power = this.attack * criticalHitChance;
    return power;
  }

  doAttack(fighter){
    const attackBlocked = this.randomInteger(0,1);
    if (attackBlocked == 0){
      let health = fighter.getHealth();
      health = health - (this.getHitPower() - fighter.getBlockPower());
      fighter.setHealth(health);
    }
    console.log(fighter.name);
    console.log(fighter.getHealth());
  }

  getHealth(){
    return this.health;
  }

  setHealth(health) {
    this.health = health;
  }

  getBlockPower(){
    let dodgeChance = this.randomInteger(1, 2);
    let power = this.defense * dodgeChance;
    return power;
  }

  randomInteger(min, max) {
    let rand = min + Math.random() * (max + 1 - min);
    rand = Math.floor(rand);
  return rand;
  }
}

export default Fighter